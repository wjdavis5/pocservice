﻿using ININ.WCF.EmeritusWDR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using SysConfig = System.Configuration;
using Timer = System.Timers.Timer;
using Trace = ININ.WCF.EmeritusWDR.Tracing.Trace;

namespace ININ.WCF.EmeritusWDR.Services
{
    public class WrapupMonitor
    {
        private int _secondsToRefresh;
        private Timer _timer;
        private List<InteractionWrapup> _wrapuplist;

        public WrapupMonitor()
        {
            using (Trace.Service.scope())
            {
                try
                {
                    _wrapuplist = new List<InteractionWrapup>();
                }
                catch (Exception e)
                {
                    Trace.Service.exception(e);
                }
            }
        }

        public void StartMonitoring()
        {
            using (Trace.Service.scope())
            {
                try
                {
                    using (var dbContext = new WrapupData())
                    {
                        _wrapuplist = dbContext.InteractionWrapups.Where(c => c.WrapupCode == "NS" || c.WrapupCode == "Wrap1" || c.WrapupCode == "Wrap2").ToList();
                    }

                    Int32.TryParse(
                         SysConfig.ConfigurationManager.AppSettings["NumberSecondsToRefresh"], out _secondsToRefresh);
                    Trace.Service.verbose("NumberSecondsToRefresh is {}", _secondsToRefresh);

                    _timer = new Timer { Interval = _secondsToRefresh * 1000 };
                    _timer.Elapsed += _timer_Elapsed;

                    _timer_Elapsed(null, null);
                    _timer.Enabled = true;
                }
                catch (Exception ex)
                {
                    Trace.Service.exception(ex);
                }
            }
        }

        internal void CleanUp()
        {
            using (Trace.Main.scope())
            {
                try
                {
                    _wrapuplist.Clear();
                    _timer.Enabled = false;
                    _timer.Elapsed -= _timer_Elapsed;
                }
                catch (Exception ex)
                {
                    Trace.Main.exception(ex);
                }
            }
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            using (Trace.Main.scope())
            {
                try
                {
                    _timer.Enabled = false;

                    ServiceLogic.WrapupDataList = _wrapuplist;
                }
                catch (ApplicationException ex)
                {
                    Trace.Main.exception(ex);
                }
                finally
                {
                    _timer.Enabled = true;
                }
            }
        }
    }
}