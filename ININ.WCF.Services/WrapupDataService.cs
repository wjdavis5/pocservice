﻿using System;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Collections.Generic;
using System.ServiceModel;
using ININ.WCF.Contracts;
using ININ.WCF.Tracing;
using Newtonsoft.Json;
using System.Configuration;

namespace ININ.WCF.Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class WrapupDataService : IWrapupDataService
    {
        private string getHoursCmd = ConfigurationManager.AppSettings["arGetHoursCmd"];
        private string getProjectMembersCmd = ConfigurationManager.AppSettings["arGetProjectMembersCmd"];
        private string emailMsgBody = ConfigurationManager.AppSettings["arEmailMsgBody"];
        
        public List<User> GetUniqueUsers()
        {
            using (Trace.Web.scope())
            {
                var tList = new List<User>();
                var conStr = ConfigurationManager.ConnectionStrings["annualReview"].ConnectionString;
                using (var conn = new SqlConnection(conStr))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(ConfigurationManager.AppSettings["arSelectUniqueUsers"], conn))
                    {
                        using (var rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                tList.Add(new User() {Displayname = rdr.GetString(1), id = rdr.GetInt32(0)});
                            }
                        }
                    }
                }
                return tList;
            }
        }

        public List<User> GetUniqueUsersXml()
        {
            return GetUniqueUsers();
        }

        public List<UserTimeEntry> GetProjectsForUser(string UserName)
        {
            using (Trace.Web.scope())
            {
                //Trace.WriteEventError(new Exception(getHoursCmd.Replace("@hID",UserName)),"" );
                var tList = new List<UserTimeEntry>();
                var conStr = ConfigurationManager.ConnectionStrings["annualReview"].ConnectionString;
                using (var conn = new SqlConnection(conStr))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(getHoursCmd.Replace("@hID", UserName), conn))
                    {

                        using (var rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                tList.Add(new UserTimeEntry()
                                    {
                                        UserDisplayName = rdr.GetString(0),
                                        Manager = rdr.GetString(1),
                                        CustomerName = rdr.GetString(2),
                                        ProjectName = rdr.GetString(3),
                                        ProjectId = rdr.GetInt32(4),
                                        HoursBilled = rdr.GetDecimal(5)
                                    });
                            }
                        }
                    }
                }
                return tList;
            }
        }

        public List<UserTimeEntry> GetUsersForProject(string projectId)
        {
            using (Trace.Web.scope())
            {
                //Trace.WriteEventError(new Exception(getHoursCmd.Replace("@hID",UserName)),"" );
                var tList = new List<UserTimeEntry>();
                var conStr = ConfigurationManager.ConnectionStrings["annualReview"].ConnectionString;
                using (var conn = new SqlConnection(conStr))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(getProjectMembersCmd.Replace("@pID", projectId), conn))
                    {

                        using (var rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                tList.Add(new UserTimeEntry()
                                    {
                                        UserDisplayName = rdr.GetString(0),
                                        EmailAddress = rdr.GetString(1),
                                        CustomerName = rdr.GetString(2),
                                        ProjectName = rdr.GetString(3),
                                        ProjectId = rdr.GetInt32(4),
                                        HoursBilled = rdr.GetDecimal(5)
                                    });
                            }
                        }
                    }
                }
                return tList;
            }
        }

        // Create new object instance using default values for constructor parameters



        public bool SendEmail(string mailMessage)
        {
            using (Trace.Web.scope())
            {
                try
                {
                    var tM = JsonConvert.DeserializeObject<MyMailMessage>(mailMessage);
                    var tempMsg = (MailMessage) tM;
                    foreach (var bccEmail in tM.Bcc)
                    {
                        tempMsg.Bcc.Add(bccEmail);
                    }
                    foreach (var ccEmail in tM.CC)
                    {
                        tempMsg.CC.Add(ccEmail);
                    }
                    tempMsg.IsBodyHtml = Convert.ToBoolean(ConfigurationManager.AppSettings["arEmailMsgBodyIsHtml"]);
                    tempMsg.Body = emailMsgBody.Replace("{{selectedUserName}}", tM.About);
                    using (var smtp = new SmtpClient("psodata1.supportlab.inin.com"))
                    {
                        smtp.Port = 25;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.Send(tempMsg);
                        Trace.Web.always("Feedback Request Email Sent For : {0}",tM.About);
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Trace.WriteEventError(ex, ex.Message);
                    Trace.Service.exception(ex);
                    return false;
                }
            }
        }

        




    }
    
}