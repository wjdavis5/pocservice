﻿using System.Threading.Tasks;
using ININ.PSO.TimeSheetMonitor;
using System;
using System.ServiceModel;
using ININ.WCF.Services;
using JsonConfig;

namespace ININ.WCF.Consumer
{
    internal class Program
    {
        static private ServiceHost _serviceHost;

        private static void Main(string[] args)
        {
            string ServiceName = Config.User.ServiceName;
            _serviceHost = new ServiceHost(typeof(WrapupDataService));
            _serviceHost.Open();
           
            //starting the timesheet monitor
            
            var tS = new TimeSheetMonitor();
            tS.MyTimer.Start();
            
            //timesheet monitor
                
           

            Console.WriteLine("Service is up and running");
            foreach (Uri u in _serviceHost.BaseAddresses)
            { Console.WriteLine("Base address: " + u.AbsoluteUri); }
            Console.WriteLine("Press enter to quit ");
            while(true)
            Console.ReadLine();
            _serviceHost.Close();
        }
    }
}