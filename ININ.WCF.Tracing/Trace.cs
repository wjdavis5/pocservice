﻿using System;

namespace ININ.WCF.Tracing
{
    // Create each trace topic class
    public class MainTopic : TopicTracer
    {
        public static int Hdl = I3Trace.initialize_topic("AnnualReview.Main", 80);

        public override int get_handle()
        {
            return Hdl;
        }
    }

    public class ServiceTopic : TopicTracer
    {
        public static int Hdl = I3Trace.initialize_topic("AnnualReview.WindowsService", 80);
         
        public override int get_handle()
        {
            return Hdl;
        }
    }

    public class StatsTopic : TopicTracer
    {
        public static int Hdl = I3Trace.initialize_topic("AnnualReview.Stat", 80);

        public override int get_handle()
        {
            return Hdl;
        }
    }

    public class Trace
    {
        // Each class should be instantiated here
        public static MainTopic Main = new MainTopic();

        public static ServiceTopic Service = new ServiceTopic();
        public static StatsTopic Stats = new StatsTopic();
        public static WebTopic Web = new WebTopic();

        public static void Initialize()
        {
            try
            {
                I3Trace.initialize_default_sinks();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Write Exception to Event Log
        /// </summary>
        public static void WriteEventError(Exception exception, string message)
        {
            using (Main.scope())
            {
                try
                {
                    Main.exception(exception, message);
                    WriteEventMessage(String.Format("{0}\r\n{1}", message, exception.Message), System.Diagnostics.EventLogEntryType.Error);
                }
                catch (Exception ex)
                {
                    Main.exception(ex);
                }
            }
        }

        /// <summary>
        /// Write Message to the Event Log
        /// </summary>
        /// <param name="sEvent">Event name</param>
        /// <param name="entryType">Event Type</param>
        public static void WriteEventMessage(string sEvent, System.Diagnostics.EventLogEntryType entryType)
        {
            using (Main.scope())
            {
                try
                {
                    const string sSource = "Service";
                    const string sLog = "Application";

                    if (!System.Diagnostics.EventLog.SourceExists(sSource))
                    {
                        System.Diagnostics.EventLog.CreateEventSource(sSource, sLog);
                    }

                    Main.status(
                        "Writing event message:" + Environment.NewLine +
                        "Source: {}" + Environment.NewLine +
                        "Log: {}" + Environment.NewLine +
                        "Type: {}" + Environment.NewLine +
                        "Message: {}" + Environment.NewLine,
                        new object[] { sSource, sLog, entryType.ToString(), sEvent });

                    System.Diagnostics.EventLog.WriteEntry(sSource, sEvent, entryType);
                }
                catch (Exception ex)
                {
                    Main.exception(ex, "Exception caught during writing event message, Exception: {0}", ex.Message);
                }
            }
        }
    }

    public class WebTopic : TopicTracer
    {
        public static int Hdl = I3Trace.initialize_topic("BlueCoatISS.WebService", 80);

        public override int get_handle()
        {
            return Hdl;
        }
    }
}