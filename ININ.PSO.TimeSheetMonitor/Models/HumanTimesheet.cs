﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ININ.PSO.TimeSheetMonitor
{
    public class HumanTimesheet
    {
        public string DisplayName { get; set; }
        public string EmailAddress { get; set; }
        public string ManagerDisplayName { get; set; }
        public string ManagerEmailAddress { get; set; }
        public int OustandingTimeSheets { get; set; }
    }
}
