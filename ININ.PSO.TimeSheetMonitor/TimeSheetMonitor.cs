﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Trace = ININ.WCF.Tracing.Trace;
using System.Configuration;

namespace ININ.PSO.TimeSheetMonitor
{
    public class TimeSheetMonitor
    {
        #region props
        public System.Timers.Timer MyTimer { get; private set; }
        public bool RanToday { get; set; }
        private readonly string conStr = ConfigurationManager.ConnectionStrings["timesheet"].ConnectionString;
        private readonly string checkTimeSheets = ConfigurationManager.AppSettings["tscheckTimeSheetsQuery"];
        private DateTime LastRunTime { get; set; }
        private readonly string emailBodySingle = ConfigurationManager.AppSettings["tsemailBodySingle"];
        private readonly string emailBodyMultiple = ConfigurationManager.AppSettings["tsemailBodyMultiple"];
        #endregion
        #region cTor
        public TimeSheetMonitor()
        {
            MyTimer = new System.Timers.Timer();
#if DEBUG
            MyTimer.Interval = 5000;
#else
            MyTimer.Interval = 55000;
#endif
            RanToday = false;
            MyTimer.Elapsed += _Timer_Elapsed;
            _Timer_Elapsed(null,null);
            MyTimer.Enabled = true;
        }
        #endregion
        #region Methods
        private void _Timer_Elapsed(object source, ElapsedEventArgs e)
        {
            using (Trace.Service.scope())
            {
                try
                {
                    MyTimer.Enabled = false;
                    if (LastRunTime.Date == DateTime.Now.Date)
                    {
                        if ((DateTime.Now.Minute != 0 || DateTime.Now.Hour != 0))
                        {
                            MyTimer.Enabled = true;
                            Trace.Main.verbose("It isnt midnight - Not sending Email");
                            return;
                        }
                    }
                    var dt = DateTime.Now;
                    foreach (var entry in GetTimeSheets())
                    {
                        if (entry.OustandingTimeSheets > 1)
                        {
                            if (dt.Hour == 0 || (LastRunTime.Date != DateTime.Now))
                            {
                                if (dt.Minute == 0 || (LastRunTime.Date != DateTime.Now))
                                {
#if (!DEBUG)
                                    SendEmailEscalated(entry);
#else
                                    Console.WriteLine("Test Escalated Email to {0}", entry.DisplayName);
#endif
                                    
                                }
                            }
                        }
                        else 
                        {
                            if (dt.Hour == 0 || (LastRunTime.Date != DateTime.Now))
                            {
                                if ((dt.Minute == 0) || (LastRunTime.Date != DateTime.Now))
                                {
#if (!DEBUG)
                                    SendEmailNormal(entry);
#else
                                    Console.WriteLine("Test Normal Email to {0}", entry.DisplayName);
#endif
                                    
                                }
                            }
                        }
                    }
                    LastRunTime = DateTime.Now;
                }
                catch (Exception ex)
                {
                    Trace.Service.exception(ex);
                }
                finally
                {
                    MyTimer.Enabled = true;
                }
            }
        }

        private SqlConnection GetConnection()
        {
            using (Trace.Service.scope())
            {
                return new SqlConnection(conStr);
            }
        }
        private IEnumerable<HumanTimesheet> GetTimeSheets()
        {
            using (Trace.Service.scope())
            {
                var tList = new List<HumanTimesheet>();
                try
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();
                        using (var cmd = new SqlCommand(checkTimeSheets, conn))
                        {
                            using (var rdr = cmd.ExecuteReader())
                            {
                                while (rdr.Read())
                                {
                                    tList.Add(new HumanTimesheet()
                                        {
                                            DisplayName = rdr.GetString(0),
                                            EmailAddress = rdr.GetString(1),
                                            ManagerDisplayName = rdr.GetString(2),
                                            ManagerEmailAddress = rdr.GetString(3),
                                            OustandingTimeSheets = rdr.GetInt32(4)
                                        });
                                }
                            }
                        }
                    }
                    return tList;
                }
                catch (Exception ex)
                {
                    Trace.WriteEventError(ex, ex.Message);
                    Trace.Main.exception(ex);
                    return new List<HumanTimesheet>();
                }
            }
        }
        private bool SendEmailNormal(HumanTimesheet humanTimesheet)
        {
            using (Trace.Service.scope())
            {
                try
                {
                    var tempMsg = new MailMessage(humanTimesheet.ManagerEmailAddress, humanTimesheet.EmailAddress)
                        {
                            Body = emailBodySingle,
                            Priority = MailPriority.High,
                            Subject = string.Format("{0} : {1}", ConfigurationManager.AppSettings["tsemailSubjectSingle"],DateTime.Now)
                        };

                    using (var smtp = new SmtpClient("psodata1.supportlab.inin.com"))
                    {
                        smtp.Port = 25;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
#if !DEBUG
                        smtp.Send(tempMsg);
#endif
                        Trace.Service.always(string.Format("Email Sent to: {0}. Outstanding Timesheets: {1}",humanTimesheet.DisplayName,humanTimesheet.OustandingTimeSheets));
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Trace.WriteEventError(ex, ex.Message);
                    Trace.Service.exception(ex);
                    return false;
                }
            }
        }
        private bool SendEmailEscalated(HumanTimesheet humanTimesheet)
        {
            using (Trace.Service.scope())
            {
                try
                {
                    var tempMsg = new MailMessage(humanTimesheet.ManagerEmailAddress, humanTimesheet.EmailAddress)
                        {
                            Body = emailBodyMultiple,
                            Priority = MailPriority.High,
                            Subject = string.Format("{0} : {1}", ConfigurationManager.AppSettings["tsemailSubjectMultiple"],DateTime.Now),
                        };
                    tempMsg.CC.Add(humanTimesheet.ManagerEmailAddress);
                    using (var smtp = new SmtpClient("psodata1.supportlab.inin.com"))
                    {
                        smtp.Port = 25;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
#if !DEBUG
                        smtp.Send(tempMsg);
#endif
                        Trace.Service.always(string.Format("Email Sent to: {0}. Outstanding Timesheets: {1}",
                                                           humanTimesheet.DisplayName,
                                                           humanTimesheet.OustandingTimeSheets));
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Trace.WriteEventError(ex, ex.Message);
                    Trace.Service.exception(ex);
                    return false;
                }
            }
        }
        #endregion
        
    }

    
}
