﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JsonConfig;

namespace ININ.TrayApplication.Main
{
    public partial class MainForm : Form
    {
        private NotifyIcon TrayIcon { get; set; }
        private ContextMenu TrayMenu { get; set; }
        private static ServiceController ServiceController { get; set; }

        public MainForm(string[] args)
            : this() //call other cTor
        {
            //Do Stuff with args if you need - the other cTor will be called automagically.

        }

        public MainForm()
        {
            
            InitializeComponent();
            ServiceController = new ServiceController(Config.MergedConfig.ServiceName);
            // Create a simple tray menu with only one item.
            TrayMenu = new ContextMenu();
            TrayMenu.MenuItems.Add("Exit", OnExit);
            TrayMenu.MenuItems.Add("Open", TrayIcon_DoubleClick);

            // Create a tray icon. In this example we use a
            // standard system icon for simplicity, but you
            // can of course use your own custom icon too.
            TrayIcon = new NotifyIcon();
            TrayIcon.Text = Config.MergedConfig.ServiceName + " Configuration";
            TrayIcon.Icon = new Icon(SystemIcons.Application, 40, 40);
            TrayIcon.DoubleClick += TrayIcon_DoubleClick;
            //var i = new Icon(Path.Combine(Directory.GetCurrentDirectory(), "flaggreen.ico"));
            TrayIcon.BalloonTipText = "Configuration Running In Tray";
            TrayIcon.BalloonTipTitle = "Configuration";
            this.ShowInTaskbar = false;
            // Add menu to tray icon and show it.
            TrayIcon.ContextMenu = TrayMenu;
            TrayIcon.Visible = true;
            MainForm_Load(null, null);

        }


        private void OnExit(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.Text = TrayIcon.Text;
            this.Hide();
            //this is really hacky - need to start the form minimized.
            Task.Factory.StartNew(() =>
                {
                    System.Threading.Thread.Sleep(30);
                    this.Invoke(new Action(() =>
                        {
                            this.WindowState = FormWindowState.Minimized;
                        }));
                });

        }

        private void TrayIcon_DoubleClick(object sender, EventArgs e)
        {
            // Show the form when the user double clicks on the notify icon. 

            // Set the WindowState to normal if the form is minimized. 
            if (this.WindowState == FormWindowState.Minimized)
                this.WindowState = FormWindowState.Maximized;

            // Activate the form. 
            this.Show();
            this.Activate();


        }


        private void MainForm_Resize(object sender, EventArgs e)
        {


            if (FormWindowState.Minimized == this.WindowState)
            {
                // mynotifyicon.Visible = true;
                this.Hide();
                TrayIcon.ShowBalloonTip(500);

            }

            else if (FormWindowState.Normal == this.WindowState)
            {
                this.Show();
            }
        }

        private void softResetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //talk over the named pipe and tell the service to reload  - service must implement IReloadable
        }

        private void stopServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsServiceRunning(Config.MergedConfig.Servicename))
                ServiceController.Stop();
        }


        private bool IsServiceRunning(string servicename)
        {
            try
            {
                ServiceController.Refresh();
                return ServiceController.Status == ServiceControllerStatus.Running;
            }
            catch (Exception ex)
            {

                throw new Exception("The service probably doesnt exist",ex);
            }
        }

        private void startServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(!IsServiceRunning(Config.MergedConfig.ServiceName))
                ServiceController.Start(new string[]{"-service"}); //start the service as... well a service
        }

        private void restartServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            stopServiceToolStripMenuItem_Click(sender, e);
            startServiceToolStripMenuItem_Click(sender,e);
        }

        private void killServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var currentProc = Process.GetCurrentProcess().Id;
            //get all processes that run with the executing assembly name, except this one
            var procs =
                Process.GetProcessesByName(Assembly.GetExecutingAssembly().GetName().Name).FirstOrDefault(p => p.Id != currentProc);
            if (procs != null) procs.Kill();
        }
    }
}


