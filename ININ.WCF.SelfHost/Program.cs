﻿using System.IO.Pipes;
using System.Web;
using System;
using System.Reflection;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Windows.Forms;
using ININ.PSO.TimeSheetMonitor;
using ININ.TrayApplication.Main;
using ININ.WCF.Services;
using ININ.WCF.Tracing;
using JsonConfig;

namespace ININ.WCF.SelfHost
{
    public class Program : ServiceBase
    {
        protected static MainForm MainForm;
        public ServiceHost ServiceHost = null;

        public Program()
        {
            ServiceName = Config.MergedConfig.ServiceName;

        }

        protected override void OnStart(string[] args)
        {
            try
            {
                Trace.Initialize();
                Trace.Main.always("Starting POC Service version: {}.", Assembly.GetExecutingAssembly().GetName().Version);
                if (ServiceHost != null)
                {
                    ServiceHost.Close();
                }

                ServiceHost = new ServiceHost(typeof(WrapupDataService));
                ServiceHost.Open();
                var tS = new TimeSheetMonitor();
                tS.MyTimer.Start();
                
            }
            catch (Exception ex)
            {
                Trace.Main.exception(ex);
                Trace.WriteEventError(ex,
                              "POC Service is unable to start due to an exception during initialization: " +
                              ex.Message);
            }

         
            
            

        }
        

        protected override void OnStop()
        {
            try
            {
                Trace.Main.always("Stopping POC.");
                if (ServiceHost == null) return;
                ServiceHost.Close();
                ServiceHost = null;
            }
            catch (Exception ex)
            {
                Trace.Main.exception(ex);
                Trace.WriteEventError(ex,
                              "POC Service is unable to stop due to an exception during initialization: " +
                              ex.Message);
            }
        }

        private static void Main(string[] args)
        {
            var config = JsonConfig.Config.MergedConfig;
            //JsonConfig.Config.SetDefaultConfig();
            if (args.Contains("-service"))
            {
                var servicesToRun = new ServiceBase[] {new Program()};
                Run(servicesToRun);
            }
            else
            {
                var a = PipeServer.Instance;
                
                MainForm = new MainForm(args);
                
                Application.Run(MainForm);
               

            }
        }
    }
}