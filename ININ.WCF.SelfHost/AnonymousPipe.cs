﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JsonConfig;
using Microsoft.Win32.SafeHandles;

namespace ININ.WCF.SelfHost
{
    public sealed class PipeServer
    {
        private NamedPipeServerStream pipeServer { get; set; }
        private static readonly PipeServer _instance = new PipeServer();
        public static PipeServer Instance { get { return _instance; } }

        private PipeServer()
        {
            pipeServer = new NamedPipeServerStream(Config.MergedConfig.ServiceName, PipeDirection.InOut, 1,
                                                   PipeTransmissionMode.Message, PipeOptions.Asynchronous);
            pipeServer.BeginWaitForConnection(ClientConnected, pipeServer);
            
        }

        private void ClientConnected(IAsyncResult result)
        {
            pipeServer.EndWaitForConnection(result);
            pipeServer.BeginWaitForConnection(ClientConnected, null);
            //Do we need to tell it to keep lisenting for connections? 
            //can we even support multiple clients?
            //pipeServer.BeginWaitForConnection(ClientConnected, null);
            if (!pipeServer.IsConnected) throw new Exception("Pipe not connected");

        }





        
    }
}
