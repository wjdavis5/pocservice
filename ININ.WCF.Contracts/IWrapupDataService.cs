﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Web;
using Newtonsoft.Json;

namespace ININ.WCF.Contracts
{
    [ServiceContract(Namespace = "WrapupDataService")]
    public interface IWrapupDataService
    {
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<User> GetUniqueUsers();

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Xml)]
        List<User> GetUniqueUsersXml();

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<UserTimeEntry> GetProjectsForUser(string UserName);
        
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<UserTimeEntry> GetUsersForProject(string projectId);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        bool SendEmail(string mailMessage);




    }

    public class User
    {
        public int id { get; set; }
        public string Displayname { get; set; }
    }

    public class UserTimeEntry
    {
        public string UserDisplayName { get; set; }
        public string EmailAddress { get; set; }
        public string Manager { get; set; }
        public string CustomerName { get; set; }
        public string ProjectName { get; set; }
        public int ProjectId { get; set; }
        public decimal HoursBilled { get; set; }
    }

    public class MyMailAddress :  System.Net.Mail.MailAddress
    {
        public MyMailAddress() : base("temp")
        {
            
        }

        [JsonConstructor]
        public MyMailAddress(string address)
            : base(address)
        {}
    }

    public class MyMailMessage : System.Net.Mail.MailMessage
    {
        public string About { get; set; }
        public new MyMailAddress From
        {
            get { return (MyMailAddress) base.From; }
            set { base.From = value; }
        }
        public new MyMailAddressCollection Bcc = new MyMailAddressCollection();
    }
    public class MyMailAddressCollection : Collection<MyMailAddress>
    {
        public MyMailAddressCollection()
       {
           
       }
    }


}